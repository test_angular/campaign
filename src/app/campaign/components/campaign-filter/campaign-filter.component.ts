import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CampaignCriteria } from '../../types/campaign-criteria.interface';
import { Brand } from '../../types/campaign.interface';

@Component({
    selector: 'app-campaign-filter',
    templateUrl: './campaign-filter.component.html',
    styleUrls: ['./campaign-filter.component.scss'],
})
export class CampaignFilterComponent {
    @Input() set criteria(criteria: CampaignCriteria) {
        if (criteria && !this.form) {
            this.form = this.initForm(criteria);
        }
    }

    @Input() brands: Brand[];
    @Output() filter: EventEmitter<CampaignCriteria> = new EventEmitter<CampaignCriteria>();

    form: FormGroup;

    constructor(private formBuilder: FormBuilder) {}

    onSubmit(form: FormGroup) {
        this.filter.emit(form.value);
    }

    clearSearch() {
        this.form.patchValue({ search: '' });
        this.filter.emit(this.form.value);
    }

    clearBrand() {
        this.filter.emit(this.form.value);
    }

    private initForm(criteria: CampaignCriteria): FormGroup {
        return this.formBuilder.group({
            search: [criteria?.search],
            brandId: [criteria?.brandId],
        });
    }
}
