import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Brand, Campaign, Media } from '../../types/campaign.interface';
import { dateForFormGroup } from '../../utils/form.utils';

@Component({
    selector: 'app-campaign-form',
    templateUrl: './campaign-form.component.html',
    styleUrls: ['./campaign-form.component.scss'],
})
export class CampaignFormComponent implements OnChanges {
    @Input() campaign: Campaign;
    @Input() brands: Brand[];
    @Input() medias: Media[];
    @Output() save: EventEmitter<Campaign> = new EventEmitter<Campaign>();

    ngOnChanges(changes: SimpleChanges): void {
        if (this.campaignAndMediasAndBrandsAreReady(changes)) {
            this.form = this.initForm(this.campaign);
        }
    }

    private campaignAndMediasAndBrandsAreReady(changes: SimpleChanges): boolean {
        return !!(
            (changes['campaign']?.currentValue || changes['brands']?.currentValue || changes['medias']?.currentValue) &&
            this.campaign &&
            this.brands &&
            this.medias
        );
    }

    form: FormGroup;

    constructor(private formBuilder: FormBuilder) {}

    onSubmit(form: FormGroup) {
        this.save.emit({
            ...form.value,
            media: form.value.media.filter((m: Media) => m.selected).map(({ selected, ...m }: Media) => m),
        });
    }

    getFormArray(formArrayName: string): FormArray {
        return this.form && (this.form.get(formArrayName) as FormArray);
    }

    private initForm(campaign: Campaign): FormGroup {
        return this.formBuilder.group({
            id: [campaign.id],
            requestId: [campaign.requestId],
            brand: [campaign.brand, Validators.required],
            campaignName: [campaign.campaignName, Validators.required],
            media: this.formBuilder.array(this.medias.map((m) => this.buildMediaFormGroup(m, campaign.media))),
            decisionDeadline: [dateForFormGroup(campaign.decisionDeadline), Validators.required],
        });
    }

    private buildMediaFormGroup(media: Media, campaignMedias: Media[]): FormGroup {
        return this.formBuilder.group({
            mediaId: [media.mediaId],
            name: [media.name],
            value: [media.value],
            selected: [!!campaignMedias.find((m) => m.mediaId === media.mediaId)],
        });
    }
}
