import { Component, Input } from '@angular/core';
import { RequestStatus } from '../../types/campaign.interface';

@Component({
    selector: 'app-campaign-status',
    templateUrl: './campaign-status.component.html',
    styleUrls: ['./campaign-status.component.scss'],
})
export class CampaignStatusComponent {
    @Input() requestStatus: RequestStatus;
    statusIcon: Record<string, string> = {
        SUBMITTED: '/assets/icons/hourglass.png',
        REJECTED: '/assets/icons/rejected.svg',
        TO_REVIEW: '/assets/icons/review.svg',
        VALIDATED: '/assets/icons/validated.svg',
        TO_MODIFY: '/assets/icons/to-modify.png',
        DRAFT: '/assets/icons/draft.png',
    };
}
