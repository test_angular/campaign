import { Component, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';

type ValidationErrorType = 'required' | 'email';

const VALIDATION_ERROR_LABELS: Record<ValidationErrorType, string> = {
    required: 'Required field',
    email: 'Invalid email',
};

@Component({
    selector: 'app-validation-error',
    templateUrl: './validation-error.component.html',
    styleUrls: ['./validation-error.component.scss'],
})
export class ValidationErrorComponent {
    errorLabels: Record<string, string> = VALIDATION_ERROR_LABELS;
    @Input() control: AbstractControl;
    @Input() customErrorLabels?: Record<string, string>;
}
