import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Campaign } from '../../types/campaign.interface';

@Component({
    selector: 'app-campaign-list',
    templateUrl: './campaign-list.component.html',
    styleUrls: ['./campaign-list.component.scss'],
})
export class CampaignListComponent {
    @Input() campaigns: Campaign[];
    @Output() edit: EventEmitter<Campaign> = new EventEmitter<Campaign>();
}
