import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-campaign-type',
    templateUrl: './campaign-type.component.html',
    styleUrls: ['./campaign-type.component.scss'],
})
export class CampaignTypeComponent {
    @Input() isAdvice: boolean = true;
}
