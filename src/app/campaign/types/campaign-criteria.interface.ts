export interface CampaignCriteria {
    search?: string;
    brandId?: number;
}
