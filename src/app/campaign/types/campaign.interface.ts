export enum CampaignStatus {
    SUBMITTED = 'SUBMITTED',
    VALIDATED = 'VALIDATED',
    REJECTED = 'REJECTED',
    PANEL_REVIEW = 'PANEL_REVIEW',
}

export interface Brand {
    brandId: number;
    name: string;
}

export interface Media {
    mediaId: number;
    name: string;
    value: string;
    selected?: boolean;
}

export interface RequestStatus {
    requestStatusId: number;
    name: string;
    value: string;
    step: number;
}

export interface Affiliate {
    affiliateId: number;
    name: string;
}

export interface User {
    userInfoId: number;
    name: string;
    email: string;
}

export interface Country {
    countryId: number;
    name: string;
    value: string;
}

export interface Campaign {
    id: string;
    requestId: number; // Numero de demande
    key: string;
    numberOfAssets: number;
    campaignName: string; // Nom de la campagne
    campaignDescription: string;
    decisionDescription: string;
    requestStatus: RequestStatus; // Statut
    advice: boolean;
    brand: Brand;
    createdDate: Date;
    updatedDate: Date;
    submittedDate: Date; // Date de soumission
    validatedDate: Date; // Date de décision
    decisionDeadline: Date;
    media: Media[];
    affiliate: Affiliate;
    createdBy: User;
    updatedBy: User;
    submittedBy: User;
    validatedBy: User | null;
    countries: Country[];
}
