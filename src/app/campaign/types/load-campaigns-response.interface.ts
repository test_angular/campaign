import { Brand, Campaign, Media } from './campaign.interface';

export interface LoadCampaignsResponse {
    campaigns: Campaign[];
    brands: Brand[];
}

export interface LoadCampaignReponse {
    campaign: Campaign;
    medias: Media[];
    brands: Brand[];
}
