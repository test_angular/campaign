import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { SubSink } from 'subsink';
import { loadCampaign, saveCampaign } from '../../store/actions/campaign.action';
import { CampaignState } from '../../store/reducers/campaign.reducers';
import { getBrands, getCampaign, getMedias } from '../../store/selectors/campaign.selectors';
import { Brand, Campaign, Media } from '../../types/campaign.interface';

@Component({
    selector: 'app-campaign-form-root',
    templateUrl: './campaign-form-root.component.html',
    styleUrls: ['./campaign-form-root.component.scss'],
})
export class CampaignFormRootComponent implements OnDestroy {
    campaign: Campaign;
    brands$: Observable<Brand[]>;
    medias$: Observable<Media[]>;

    private subs = new SubSink();

    constructor(private campaignStore: Store<CampaignState>, private route: ActivatedRoute) {}

    ngOnInit(): void {
        this.subs.sink = this.campaignStore.pipe(select(getCampaign)).subscribe((campaign) => (this.campaign = campaign));
        this.brands$ = this.campaignStore.pipe(select(getBrands));
        this.medias$ = this.campaignStore.pipe(select(getMedias));
        const requestId: number = Number(this.route.snapshot.paramMap.get('requestId'));
        this.campaignStore.dispatch(loadCampaign({ payload: requestId }));
    }

    onSave(campaign: Campaign) {
        this.campaignStore.dispatch(
            saveCampaign({
                payload: {
                    ...this.campaign,
                    ...campaign,
                },
            })
        );
    }

    ngOnDestroy(): void {
        this.subs.unsubscribe();
    }
}
