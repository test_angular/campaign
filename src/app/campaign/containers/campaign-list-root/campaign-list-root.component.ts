import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { loadCampaigns } from '../../store/actions/campaign.action';
import { CampaignState } from '../../store/reducers/campaign.reducers';
import { getBrands, getCampaigns } from '../../store/selectors/campaign.selectors';
import { CampaignCriteria } from '../../types/campaign-criteria.interface';
import { Brand, Campaign } from '../../types/campaign.interface';

@Component({
    selector: 'app-campaign-list-root',
    templateUrl: './campaign-list-root.component.html',
    styleUrls: ['./campaign-list-root.component.scss'],
})
export class CampaignListRootComponent {
    campaigns$: Observable<Campaign[]>;
    criteria: CampaignCriteria = {};
    brands$: Observable<Brand[]>;

    constructor(private campaignStore: Store<CampaignState>, private router: Router) {}

    ngOnInit(): void {
        this.campaigns$ = this.campaignStore.pipe(select(getCampaigns));
        this.brands$ = this.campaignStore.pipe(select(getBrands));
        this.campaignStore.dispatch(loadCampaigns({ payload: this.criteria }));
    }

    onFilter(criteria: CampaignCriteria) {
        this.criteria = criteria;
        this.campaignStore.dispatch(loadCampaigns({ payload: this.criteria }));
    }

    onEdit(campaign: Campaign) {
        this.router.navigate(['/campaign/form', campaign.requestId]);
    }
}
