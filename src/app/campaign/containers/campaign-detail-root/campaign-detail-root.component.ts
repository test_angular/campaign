import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { saveCampaign } from '../../store/actions/campaign.action';
import { CampaignState } from '../../store/reducers/campaign.reducers';
import { Campaign } from '../../types/campaign.interface';

@Component({
    selector: 'app-campaign-detail-root',
    templateUrl: './campaign-detail-root.component.html',
    styleUrls: ['./campaign-detail-root.component.scss'],
})
export class CampaignDetailRootComponent {
    campaign: Campaign;

    constructor(private campaignStore: Store<CampaignState>, private router: Router) {}

    ngOnInit(): void {}

    onSubmit(campaign: Campaign) {
        this.campaignStore.dispatch(
            saveCampaign({
                payload: campaign,
            })
        );
        this.router.navigate(['/campaign/confirmation']);
    }
}
