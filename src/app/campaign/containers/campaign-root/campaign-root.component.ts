import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CampaignState } from '../../store/reducers/campaign.reducers';

@Component({
    selector: 'app-campaign-root',
    templateUrl: './campaign-root.component.html',
    styleUrls: ['./campaign-root.component.scss'],
})
export class CampaignRootComponent implements OnInit {
    canGoToConfirmationStep$: Observable<boolean>;

    constructor(private campaignStore: Store<CampaignState>) {}

    ngOnInit(): void {}
}
