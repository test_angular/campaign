import { createAction, props } from '@ngrx/store';
import { CampaignCriteria } from '../../types/campaign-criteria.interface';
import { Campaign } from '../../types/campaign.interface';
import { LoadCampaignReponse, LoadCampaignsResponse } from '../../types/load-campaigns-response.interface';

export const loadCampaigns = createAction('[Campaign] Load Campaigns', props<{ payload: CampaignCriteria }>());

export const loadCampaignsSuccess = createAction(
    '[Campaign] Load Campaigns Success',
    props<{ payload: LoadCampaignsResponse }>()
);

export const loadCampaignsFail = createAction('[Campaign] Load Campaigns Fail');

export const loadCampaign = createAction('[Campaign] Load Campaign', props<{ payload: number }>());

export const loadCampaignSuccess = createAction('[Campaign] Load Campaign Success', props<{ payload: LoadCampaignReponse }>());

export const loadCampaignFail = createAction('[Campaign] Load Campaign Fail');

export const saveCampaign = createAction('[Campaign] Save Campaign', props<{ payload: Campaign }>());

export const saveCampaignSuccess = createAction('[Campaign] Save Campaign Success', props<{ payload: Campaign }>());

export const saveCampaignFail = createAction('[Campaign] Save Campaign Fail');
