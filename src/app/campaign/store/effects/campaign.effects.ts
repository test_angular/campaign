import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, of, switchMap } from 'rxjs';
import { CampaignMockService } from '../../services/campaign-mock.service';
import { CampaignCriteria } from '../../types/campaign-criteria.interface';
import { Campaign } from '../../types/campaign.interface';
import {
    loadCampaign,
    loadCampaignFail,
    loadCampaignSuccess,
    loadCampaigns,
    loadCampaignsFail,
    loadCampaignsSuccess,
    saveCampaign,
    saveCampaignFail,
    saveCampaignSuccess,
} from '../actions/campaign.action';

@Injectable()
export class CampaignEffects {
    constructor(private action$: Actions, private campaignService: CampaignMockService, private router: Router) {}

    loadCampaigns$ = createEffect(() =>
        this.action$.pipe(
            ofType(loadCampaigns),
            switchMap((props: { payload: CampaignCriteria }) =>
                this.campaignService.loadCampaigns(props.payload).pipe(
                    map((response) => loadCampaignsSuccess({ payload: response })),
                    catchError(() => of(loadCampaignsFail()))
                )
            )
        )
    );

    loadCampaign$ = createEffect(() =>
        this.action$.pipe(
            ofType(loadCampaign),
            switchMap((props: { payload: number }) =>
                this.campaignService.loadCampaign(props.payload).pipe(
                    map((response) => loadCampaignSuccess({ payload: response })),
                    catchError(() => of(loadCampaignFail()))
                )
            )
        )
    );

    saveCampaign$ = createEffect(() =>
        this.action$.pipe(
            ofType(saveCampaign),
            switchMap((props: { payload: Campaign }) =>
                this.campaignService.saveCampaign(props.payload).pipe(
                    map((response) => {
                        this.router.navigate(['/campaign/list']);
                        return saveCampaignSuccess({ payload: response });
                    }),
                    catchError((error) => of(saveCampaignFail()))
                )
            )
        )
    );
}
