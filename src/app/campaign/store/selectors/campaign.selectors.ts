import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CampaignState } from '../reducers/campaign.reducers';

const getCampaignState = createFeatureSelector<CampaignState>('campaign');

export const getCampaigns = createSelector(getCampaignState, (state: CampaignState) => state.campaigns);

export const getBrands = createSelector(getCampaignState, (state: CampaignState) => state.brands);

export const getMedias = createSelector(getCampaignState, (state: CampaignState) => state.medias);

export const getCampaign = createSelector(getCampaignState, (state: CampaignState) => state.campaign);
