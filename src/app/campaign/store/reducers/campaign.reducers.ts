import { Action, createReducer, on } from '@ngrx/store';
import { Brand, Campaign, Media } from '../../types/campaign.interface';
import { LoadCampaignReponse, LoadCampaignsResponse } from '../../types/load-campaigns-response.interface';
import { loadCampaignSuccess, loadCampaignsSuccess, saveCampaignSuccess } from '../actions/campaign.action';

export interface CampaignState {
    campaigns: Campaign[];
    campaign: Campaign;
    brands: Brand[];
    medias: Media[];
}

const initialState: CampaignState = {
    campaigns: [],
    campaign: null,
    brands: [],
    medias: [],
};

const loadCampaignsSuccessReducer = (state: CampaignState, props: { payload: LoadCampaignsResponse }) => ({
    ...state,
    campaigns: props.payload.campaigns,
    brands: props.payload.brands,
});

const loadCampaignSuccessReducer = (state: CampaignState, props: { payload: LoadCampaignReponse }) => ({
    ...state,
    campaign: props.payload.campaign,
    medias: props.payload.medias,
    brands: props.payload.brands,
});

const saveCampaignSuccessReducer = (state: CampaignState, props: { payload: Campaign }) => {
    return {
        ...state,
        campaign: props.payload,
    };
};

const reducer = createReducer(
    initialState,
    on(saveCampaignSuccess, saveCampaignSuccessReducer),
    on(loadCampaignsSuccess, loadCampaignsSuccessReducer),
    on(loadCampaignSuccess, loadCampaignSuccessReducer)
);

export function campaignReducer(state: CampaignState | undefined, action: Action) {
    return reducer(state, action);
}
