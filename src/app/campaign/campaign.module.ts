import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { NgxMaskDirective, NgxMaskPipe, provideNgxMask } from 'ngx-mask';
import { CampaignRoutingModule } from './campaign-routing.module';
import { CampaignFilterComponent } from './components/campaign-filter/campaign-filter.component';
import { CampaignFormComponent } from './components/campaign-form/campaign-form.component';
import { CampaignListComponent } from './components/campaign-list/campaign-list.component';
import { ValidationErrorComponent } from './components/validation-error/validation-error.component';
import { CampaignFormRootComponent } from './containers/campaign-form-root/campaign-form-root.component';
import { CampaignListRootComponent } from './containers/campaign-list-root/campaign-list-root.component';
import { CampaignRootComponent } from './containers/campaign-root/campaign-root.component';
import { CampaignMockService } from './services/campaign-mock.service';
import { CampaignEffects } from './store/effects/campaign.effects';
import { campaignReducer } from './store/reducers/campaign.reducers';
import { CampaignTypeComponent } from './components/campaign-type/campaign-type.component';
import { CampaignStatusComponent } from './components/campaign-status/campaign-status.component';

@NgModule({
    declarations: [
        ValidationErrorComponent,
        CampaignFormRootComponent,
        CampaignListRootComponent,
        CampaignRootComponent,
        CampaignListComponent,
        CampaignFormComponent,
        CampaignFilterComponent,
        CampaignTypeComponent,
        CampaignStatusComponent,
    ],
    imports: [
        CommonModule,
        CampaignRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        StoreModule.forFeature('campaign', campaignReducer),
        EffectsModule.forFeature([CampaignEffects]),
        NgxMaskDirective,
        NgxMaskPipe,
        NgSelectModule,
    ],
    providers: [provideNgxMask(), CampaignMockService],
})
export class CampaignModule {}
