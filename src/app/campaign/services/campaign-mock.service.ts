import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { BRANDS_MOCK } from '../models/brands.mock';
import { MEDIAS_MOCK } from '../models/medias.mock';
import { CampaignCriteria } from '../types/campaign-criteria.interface';
import { Campaign } from '../types/campaign.interface';
import { LoadCampaignReponse, LoadCampaignsResponse } from '../types/load-campaigns-response.interface';

@Injectable({
    providedIn: 'root',
})
export class CampaignMockService {
    private campaignsUrl = 'api/campaigns/';

    constructor(private http: HttpClient) {}

    loadCampaigns(criteria: CampaignCriteria): Observable<LoadCampaignsResponse> {
        return this.http.get<Campaign[]>(this.campaignsUrl).pipe(
            map((campaigns) => ({
                campaigns: this.getFilteredCampaigns(campaigns, criteria),
                brands: BRANDS_MOCK,
            }))
        );
    }

    loadCampaign(campaignId: number): Observable<LoadCampaignReponse> {
        return this.http.get<Campaign>(this.campaignsUrl + campaignId).pipe(
            map((campaign) => ({
                campaign,
                brands: BRANDS_MOCK,
                medias: MEDIAS_MOCK,
            }))
        );
    }

    saveCampaign(campaign: Campaign): Observable<Campaign> {
        return this.http.put(this.campaignsUrl + campaign.id, campaign).pipe(map(() => campaign));
    }

    private getFilteredCampaigns(campaigns: Campaign[], criteria: CampaignCriteria): Campaign[] {
        return campaigns.filter((c) => {
            if (criteria.brandId && c.brand.brandId !== criteria.brandId) {
                return false;
            }
            if (criteria.search) {
                if (
                    c.campaignName.toLowerCase().includes(criteria.search.toLowerCase()) ||
                    c.campaignDescription.toLowerCase().includes(criteria.search.toLowerCase()) ||
                    c.requestStatus.value.toLowerCase().includes(criteria.search.toLowerCase())
                ) {
                    return true;
                }
                return false;
            }
            return true;
        });
    }
}
