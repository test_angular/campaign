import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { CAMPAIGNS_MOCK } from '../models/campaigns.mock';

@Injectable({
    providedIn: 'root',
})
export class CampaignDataService implements InMemoryDbService {
    constructor() {}
    createDb() {
        return {
            campaigns: CAMPAIGNS_MOCK,
        };
    }
}
