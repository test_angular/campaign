import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CampaignFormRootComponent } from './containers/campaign-form-root/campaign-form-root.component';
import { CampaignListRootComponent } from './containers/campaign-list-root/campaign-list-root.component';
import { CampaignRootComponent } from './containers/campaign-root/campaign-root.component';

const routes: Routes = [
    {
        path: '',
        component: CampaignRootComponent,
        children: [
            {
                path: '',
                redirectTo: 'list',
                pathMatch: 'full',
            },
            {
                path: 'list',
                component: CampaignListRootComponent,
            },
            {
                path: 'form/:requestId',
                component: CampaignFormRootComponent,
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CampaignRoutingModule {}
