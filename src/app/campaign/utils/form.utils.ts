import { DatePipe } from '@angular/common';

export const dateForFormGroup = (date: Date | string): string | null => {
    if (date) {
        const dp = new DatePipe(navigator.language);
        return dp.transform(new Date(date), 'y-MM-dd');
    } else {
        return null;
    }
};
