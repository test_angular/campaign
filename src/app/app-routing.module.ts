import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    { path: '', redirectTo: 'campaign', pathMatch: 'full' },
    {
        path: 'campaign',
        loadChildren: () => import('./campaign/campaign.module').then((m) => m.CampaignModule),
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
